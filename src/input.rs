use anyhow::{bail, Result};
use dialoguer::console;
use dialoguer::Password;
use pinentry::PassphraseInput;
use secrecy::ExposeSecret;

pub trait PassphraseInputPrompt {
    fn with_description(&mut self, description: String);
    fn with_prompt(&mut self, prompt: String);
    fn with_error(&mut self, error: String);
    fn with_confirm(&mut self, confirm: String);
    fn with_required(&mut self, required: bool);
    fn read_secret(&mut self) -> Result<String>;
}

pub struct PinentryPassphraseInput {
    description: String,
    prompt: String,
    error: Option<String>,
    confirm: Option<String>,
    required: bool,
}

impl PinentryPassphraseInput {
    pub fn new() -> Self {
        PinentryPassphraseInput {
            description: String::new(),
            prompt: String::new(),
            error: None,
            confirm: None,
            required: false,
        }
    }
}

impl PassphraseInputPrompt for PinentryPassphraseInput {
    fn with_description(&mut self, description: String) {
        self.description = description;
    }

    fn with_prompt(&mut self, prompt: String) {
        self.prompt = prompt;
    }

    fn with_error(&mut self, error: String) {
        self.error = Some(error);
    }

    fn with_confirm(&mut self, confirm: String) {
        self.confirm = Some(confirm)
    }

    fn with_required(&mut self, required: bool) {
        self.required = required;
    }

    fn read_secret(&mut self) -> Result<String> {
        let mut input = match PassphraseInput::with_default_binary() {
            None => bail!("pinentry not found"),
            Some(i) => i,
        };
        let mismatch_error = "Passphrases don't match";
        let empty_error = "Passphrase is required";
        input
            .with_description(&self.description)
            .with_prompt(&self.prompt)
            .with_timeout(30);
        if let Some(confirm_prompt) = &self.confirm {
            input.with_confirmation(&confirm_prompt, &mismatch_error);
        }
        if let Some(error) = &self.error {
            input.with_error(&error);
        }
        if self.required {
            input.required(empty_error);
        }
        match input.interact() {
            Err(e) => bail!("Pinentry error: {}", e),
            Ok(s) => Ok(s.expose_secret().to_owned()),
        }
    }
}

pub struct TerminalPassphraseInput {
    description: String,
    prompt: String,
    error: Option<String>,
    confirm: Option<String>,
    required: bool,
}

impl TerminalPassphraseInput {
    pub fn new() -> Self {
        TerminalPassphraseInput {
            description: String::new(),
            prompt: String::new(),
            error: None,
            confirm: None,
            required: false,
        }
    }
}

impl PassphraseInputPrompt for TerminalPassphraseInput {
    fn with_description(&mut self, description: String) {
        self.description = description;
    }

    fn with_prompt(&mut self, prompt: String) {
        self.prompt = prompt;
    }

    fn with_error(&mut self, error: String) {
        self.error = Some(error);
    }

    fn with_confirm(&mut self, confirm: String) {
        self.confirm = Some(confirm)
    }

    fn with_required(&mut self, required: bool) {
        self.required = required;
    }

    fn read_secret(&mut self) -> Result<String> {
        if let Some(confirm_prompt) = &self.confirm {
            println!("{}", self.description);
            let passphrase = Password::new()
                .with_prompt(&self.prompt)
                .with_confirmation(confirm_prompt, "Passphrases don't match")
                .allow_empty_password(!self.required)
                .interact()?;
            Ok(passphrase)
        } else {
            if let Some(error) = &self.error {
                println!("{}", console::style(error).red());
            }
            println!("{}", self.description);
            let passphrase = Password::new().with_prompt(&self.prompt).interact()?;
            Ok(passphrase)
        }
    }
}

pub fn pinentry_available() -> bool {
    PassphraseInput::with_default_binary().is_some()
}
